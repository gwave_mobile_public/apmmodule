# APMModule

[![CI Status](https://img.shields.io/travis/yangpeng/APMModule.svg?style=flat)](https://travis-ci.org/yangpeng/APMModule)
[![Version](https://img.shields.io/cocoapods/v/APMModule.svg?style=flat)](https://cocoapods.org/pods/APMModule)
[![License](https://img.shields.io/cocoapods/l/APMModule.svg?style=flat)](https://cocoapods.org/pods/APMModule)
[![Platform](https://img.shields.io/cocoapods/p/APMModule.svg?style=flat)](https://cocoapods.org/pods/APMModule)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

APMModule is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'APMModule'
```

## Author

yangpeng, peng.yang@kikitrade.com

## License

APMModule is available under the MIT license. See the LICENSE file for more info.
