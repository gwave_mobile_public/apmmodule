//
//  APMAppDelegate.h
//  APMModule
//
//  Created by yangpeng on 09/23/2022.
//  Copyright (c) 2022 yangpeng. All rights reserved.
//

@import UIKit;

@interface APMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
