//
//  main.m
//  APMModule
//
//  Created by yangpeng on 09/23/2022.
//  Copyright (c) 2022 yangpeng. All rights reserved.
//

@import UIKit;
#import "APMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([APMAppDelegate class]));
    }
}
